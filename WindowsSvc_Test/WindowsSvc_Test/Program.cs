﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSvc_Test
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new LxmWinService()
            };
            ServiceBase.Run(ServicesToRun);
#if DEBUG
            //Form1 from = new Form1();
            //from.ShowDialog();
#endif
        }
    }
}
