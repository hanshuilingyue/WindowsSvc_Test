﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSvc_Test
{
    partial class LxmWinService : ServiceBase
    {
        public LxmWinService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: 在此处添加代码以启动服务。
            WinTasks.Start();
        }

        //停止服务
        protected override void OnStop()
        {
            // TODO: 在此处添加代码以执行停止服务所需的关闭操作。
            WriteLog("服务已关闭：" + DateTime.Now.ToString());
        }

        //服务暂停执行代码
        protected override void OnPause()
        {
            WriteLog("服务暂停：" + DateTime.Now.ToString());
            base.OnPause();
        }

        //服务恢复执行代码
        protected override void OnContinue()
        {
            WriteLog("服务恢复：" + DateTime.Now.ToString());
            base.OnContinue();
        }

        //系统即将关闭执行代码
        protected override void OnShutdown()
        {
            WriteLog("系统即将关闭：" + DateTime.Now.ToString());
            base.OnShutdown();
        }

        //写入txt文档，用于调试日志
        public void WriteLog(string str)
        {
            FileInfo serverlog = new FileInfo(@"D:\WindowServerlog.txt");
            if (!serverlog.Exists)
            {
                serverlog.Create();
            }
            using (StreamWriter sw = File.AppendText(@"D:\WindowServerlog.txt"))
            {
                sw.WriteLine(str);
                sw.Flush();
            }
        }

    }
}
