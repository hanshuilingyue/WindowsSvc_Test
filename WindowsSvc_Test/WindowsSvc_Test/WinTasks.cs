﻿#region
// ----------------------------------------------------------------
// 文 件 名：WinTasks
// 描  述：
//
// 创建标识：
//
// 修改标识：
// 修改描述：
//
// 修改标识：
// 修改描述：
//
// 备  注：
// ----------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindowsSvc_Test
{
    public class WinTasks
    {

        /// <summary>
        /// 开启线程启动服务
        /// </summary>
        public static void Start()
        {
            Thread thread = new Thread(WinServiceTest.DoSomeThing);
            thread.Start();
        }

    }

    internal class WinServiceTest
    {
        public static void DoSomeThing()
        {
            //TODO: 服务执行内容。
            FileInfo configFile = new FileInfo(@"D:\log4net.config");
            if (!configFile.Exists)
            {
                using (StreamWriter sw = File.CreateText(configFile.FullName))
                {
                    sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                    sw.WriteLine("<configuration>");
                    sw.WriteLine("  <log4net>");
                    sw.WriteLine("    <root>");
                    sw.WriteLine("      <level value=\"ALL\"/>");
                    sw.WriteLine("       <appender-ref ref=\"MyLogger_1\" />");
                    sw.WriteLine("   </root>");
                    sw.WriteLine("    <appender name=\"MyLogger_1\" type=\"log4net.Appender.RollingFileAppender\">");
                    sw.WriteLine("      <param name=\"File\" value=\"Logs/log_\"/>");
                    sw.WriteLine("      <param name=\"AppendToFile\" value=\"true\"/>");
                    sw.WriteLine("      <param name=\"RollingStyle\" value=\"Composite\"/>");
                    sw.WriteLine("      <param name=\"DatePattern\" value=\"yyyy-MM-dd&quot;.txt&quot;\"/>");
                    sw.WriteLine("      <param name=\"MaximumFileSize\" value=\"3MB\"/>");
                    sw.WriteLine("      <param name=\"MaxSizeRollBackups\" value=\"100\"/>");
                    sw.WriteLine("      <param name=\"StaticLogFileName\" value=\"false\"/>");
                    sw.WriteLine("      <lockingModel type=\"log4net.Appender.FileAppender+MinimalLock\" />");
                    sw.WriteLine("      <layout type=\"log4net.Layout.PatternLayout\">");
                    sw.WriteLine("        <param name=\"ConversionPattern\" value=\"[时间]:%d [级别]:%p [线程]:%t%n[内容]:%m%n%n\"/>");
                    sw.WriteLine("      </layout>");
                    sw.WriteLine("    </appender>");
                    sw.WriteLine("  </log4net>");
                    sw.WriteLine("</configuration>");
                    sw.Close();
                }
            }
        }
    }

}
